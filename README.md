

# perceptual_knowledge_base

#### Pre-introduction
This is a snapshot of a small project I am working on in order to develop ways of improving system decoupling in high-level robot systems architecture, achieve a richer range of object representations in perception-action systems, all with the end goal of reducing the amount of code we have to write, and the things we have to handle and keep track of, in order to make robots do interesting stuff.

#### Introduction
Mobile service robots live in a spatially-situated world, where information about objects in the environment must be detected, stored, communicated, retreived and manipulated between systems during pursuit of high-level tasks such as object search, pick-and-place or manipulation of physical artefacts like doors and buttons.

A typical service robot may be able to detect numerous types of objects using generic object detectors, or specialised detectors for particular objects or *parts* of objects (i.e. a car wheel, a door handle, or an elevator panel). Other detectors, such as velocity and elevation sensors, or systems performing other calculations, or semantic annotators, are often also used in order to improve the richness of object representations. Principled systems defining how this range of information about objects is stored and retreived, and APIs surrounding how it is *used* (i.e. by a control layer) are commonly missing from many robot system architectures. Smaller, or ad-hoc, systems often tangle perception and control, leaving the developer of the control system to manage the triggering of detectors, the interpretation of their output, their eventual shutdown, as well as the annotation of any semantic data and persistence of any knowledge gained during a session. This creates unnecessary dependencies, extra mental overhead for behaviour developers, and can lead to monolithic control code with far too great a range of responsibilities.

If a robot software system is to scale, then developers of robot control systems must be able to retreive object-centric information in simple, uniform ways across different behaviours, and developers of perception and annotation systems must have a clear API to provide their results to the rest of a robot system. In this way we can work towards optimising robot behavioural code such that it is leaner, simpler and more powerful, improving testability and debugging work, and acheiving a better decoupling of systems.

This software functions as a knowledge base for storing perceptual data and providing object-focused functionality to control system developers, and the developers of object detectors, via APIs that are underpinned by ROS. It is intended to sit as a software layer inbetween a perception layer -- such as object detectors -- and a control layer -- such as planners, low-level execution modules, or monitors -- to provide communication between these layers, act as a common datastore, and also provide the option to *abstract away* processing of information through the use of *live* object models.

#### Headline Functions

- Generic knowledge base for storing, updating and querying [component-based](https://en.wikipedia.org/wiki/Entity_component_system) representations of objects in a robot's environment. Component-based structure optionally allows for composite objects populated from the output of specialised part-detectors.
- Object API allows apriori information to be stored (such as initial pose, label, type etc.) as well as information received on-line from detectors or other sources. Objects can optionally function as a [blackboard](https://en.wikipedia.org/wiki/Blackboard_system), and may hold temporary data, or data intended to be read by other systems (such as visualisers, tablet apps etc.), and the results of processing. Initial object states can be defined in plain-text yaml, and object data is arbitrarily editable on-line, for testing and debugging purposes.
- Objects may be functional, *live models*, performing on-the-fly or on-demand calculations about object data, for instance, we may combine the results of multiple detectors and store the result, or select only the one with the highest confidence from a bank of detectors. Or, automatically transform an object's pose into an arbitrary, user-requested *tf* frame. These sorts of functions are often found embedded in control code.
- Event API that allows a control layer to receive *asynchronous* events when information -- such as pose, detector confidence, or other on-the-fly calculated fields -- has arrived in an object model from information annotators. This additionally allows objects to be connected to a visualiser, such as RVIZ, or other front-end, and to update their visualisation automatically as they are updated from detectors or other sources.
- No single-process assumption. Any ROS node can store and retreive information about objects, as well as receive event notifications about changes made to objects. This is useful for detector developers, allowing them to largely decouple from the rest of the system if they so choose.
- A C++ interface is provided that wraps all ROS code and is designed to be portable and dependency-light, to allow use by nodes on other network devices or environments.


#### High-level architecture diagram

![basic system architecture](glue.png)

#### Technical Specs
Developed under ROS Melodic, using C++17
##### To Compile
Clone this repository into a catkin workspace and call `catkin_make`
##### To Run
To launch the main knowledge base node, you can run:
````roslaunch knowledge_base launch_kb.launch````
This will launch the node, and populate the knowledge base with some dummy data from the `test_objects.yaml` file.

A test node is also compiled, `test_kb_interface`, which you can run in order to demonstrate basic functionalities. The source code for this file under `/knowledge_interface/test/test_kb_interface.cpp`is documented and details the different steps required to perform basic knowledge base operations.

#### Worked Example: Soup-microwaving robot
Imagine a robot situated in a human workplace, such as an office. The job of the robot is to microwave soup for its colleagues. There are two kitchens in the environment, each with a microwave, that the robot may use in this task. One microwave is a newer model, with a touch-screen button controller, with the door opening to the left. The other microwave is an older model, with a dial that must be turned, and with the door opening to the right.

In a hierarchical, inheritance-based system, representing these microwaves as objects in our robot brain would likely give us some kind of class hierarchy, with a base-class `Microwave`which we inherit into child classes that give us the specialisation for the two different types we are handling.

A component-based model lets us represent this difference a little more easily. We instantiate two `Microwave`objects, both with `MicrowaveDoor`components, but one with a`MicrowaveButtonsController`component, and the other with a `MicrowaveDialController`component. It is likely that each of these controls requires a specific detector or some kind of information annotator that provides us the state of the control -- which buttons are pressed, the angle of the dial, or other things a robot might need to know before it interacts with the control.

The sequence of our task then proceeds as follows:
1. A task is instantiated from the control layer, such as `use kitchen_7_microwave`
2. A Starting signal is sent through to the perceptual knowledge base using the knowledge interface
3. Each component of the object labeled`kitchen_7_microwave`receives a starting signal
4. In turn, each component sends a starting signal on to any information annotators or detectors or other systems required to begin populating data about that component. I.e. here we could either start a detector for buttons or the dial controller.
5. The perceptual knowledge base is updated with the results from these information annotators.
		5a. This can be a continuous update, if the annotators support this, or a on-demand, one-shot update.
		5b. Components are also *live* and may perform post-processing on this data, for instance comparing the results of two detectors and providing only the one with the higher confidence measure, or comparing their output to some known ground-truth value.
6. The control layer reads from the perceptual knowledge base, and uses this information to proceed with executing its task.
	6a. Here the robot may also want to branch behaviours based on the components the  `kitchen_7_microwave`may have, i.e. it may wish to choose a different control path for interacting with a dial or buttons.
7. A stop signal is sent when the control layer is satisfied the task is finished, causing object models, components, and information annotators to shut down cleanly.

This is illustrated a bit more with the following diagram

![illustration of microwave example](microwave_flow.png)

#### yaml object specification

Objects can be specified in plaintext with yaml. These templates are then used to instantiate C++ objects with relevant functionality. This allows us to set initial states and values that we might want to use, or overwrite, later. The yaml specification looks like this (the below file is located in `knowledge_interface/test/test_objects.yaml`): 


```
ObjectInstances: # the root namespace for instances of known objects
  kitchen_4_microwave: # the unique label of this particular object -- a microwave with button controls
    Components: # the components that make up this object
      Common: # data common to all objects, such as initial pose in map frame, TODO: Could probably be moved up a level
        type: Microwave # the type of this particular object
        pose: [1.6, 1.33, 0.00, 0.01, -0.06, 0.70, 0.22]
        floor: 1
      MicrowaveDoor:
        type: MicrowaveDoor
        open: false
        open_direction: "left"
      MicrowaveButtonsController:
        type: MicrowaveButtonsController
  kitchen_7_microwave:  # the unique label of this particular object -- a microwave with dial controls
    Components:
      Common:
        type: Microwave
        pose: [3.6, 2.33, 0.00, 0.01, -0.06, 0.70, 0.22]
        floor: 3
      MicrowaveDoor:
        type: MicrowaveDoor
        open: true
        open_direction: "right"
      MicrowaveDialController:
        type: MicrowaveDialController
        state: 0.5
```
