cmake_minimum_required(VERSION 3.0.2)
project(knowledge_base_msgs)

## Compile as C++17
set(CMAKE_CXX_STANDARD 17)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED
  COMPONENTS
  message_generation
  std_msgs
  geometry_msgs
)
add_message_files(
  FILES
  Component.msg
  ComponentDataField.msg
  ComponentDataHeader.msg
  DispatchEvent.msg
)
add_service_files(
  FILES
  GetComponentDataField.srv
  SetComponentDataField.srv
  GetIsModelStarted.srv
  StartStopModel.srv
)
## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
  geometry_msgs
)
catkin_package(
  CATKIN_DEPENDS message_runtime std_msgs geometry_msgs
)
###########
## Build ##
###########
## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  ${catkin_INCLUDE_DIRS}
)
