#pragma once
#include <string>
#include <object_structure/components/base_component.h>

namespace glue {
namespace knowledge_base {
namespace components {
class MicrowaveControlButtons : public BaseComponent {
 public:
  MicrowaveControlButtons(const std::string &component_label, const std::string &parent_label);
  void Visualise() override;
  bool HandleStart() override;
  bool HandleStop() override;
};
}  // namespace components
}  // namespace knowledge_base
}  // namespace glue