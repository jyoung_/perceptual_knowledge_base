#pragma once
// system includes
#include <boost/signals2/signal.hpp>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
// glue includes
#include <knowledge_interface/event_dispatcher.h>
#include <knowledge_interface/interface_types.h>
// ros includes
#include <tf2_ros/transform_listener.h>

namespace glue {
namespace knowledge_base {
namespace components {
/**
 * @brief The main base-class for all components. Provides all basic functionality
 *        such as data storage and event triggers.
 */
class BaseComponent {
 public:
  BaseComponent(const std::string &component_label, const std::string &parent_label)
      : label_(component_label), parent_(parent_label), started_(false), dispatch_events_(false), tf_buffer_() {}
  virtual ~BaseComponent() = default;

  /**
   * @brief Visualise this component in RVIZ.
   */
  virtual void Visualise();

  /**
   * @brief Starts this component,
   * @return true if the component could start, false if it couldn't.
   */
  bool Start();

  /**
   * @brief Stops this component,
   * @return true if the component could stop, false if it couldn't.
   */
  bool Stop();

  /**
   * @brief Start method intended to be overridden by child components to implement
   *        component-specific start logic if required.
   * @return whether the start was successful or not
   */
  virtual bool HandleStart();

  /**
   * @brief Stop method intended to be overridden by child components to implement
   *        component-specific stop logic if required.
   * @return whether the stop was successful or not
   */
  virtual bool HandleStop();

  /**
   * @brief Sets the friendly label of this component, i.e. "DoorNfcLockFront", "DoorPanel"
   * @param component_label - the label of this component
   */
  void SetLabel(const std::string &component_label) { label_ = component_label; }

  /**
   * @brief Returns the name of this component.
   * @returns label of this component
   */
  std::string GetLabel() const { return label_; }

  /**
   * @brief Sets the name of the object this component is attached to (referred to as its parent)
   * @param parent_label - the label of the object that owns this component
   */
  void SetParentLabel(const std::string &parent_label) { parent_ = parent_label; }

  /**
   * @brief Returns the name of the object this component is attached to.
   * @returns parent_ - the label of the object that owns this component
   */
  std::string GetParentLabel() const { return parent_; }

  /**
   * @brief Returns whether this component is started or not
   * @returns true if this component is started, false if it is not
   */
  bool HasStarted() const { return started_; }

  /**
   * @brief Public-facing method to update the data in this component if possible
   * @param data_field_name The name of the data field to set
   * @param data_field_data The data to put in, one of the ComponentDataType types
   *                        wrapped in a DataFrame
   */
  void SetDataField(const std::string &data_field_name, const DataFrame<ComponentDataType> &data_field_data);

  /**
   * @brief Public-facing method to retrieve data from this component if possible
   * @param data_field_name The name of the data field to set
   * @param TF frame to transform the returned data into before returning
   */
  DataFrame<ComponentDataType> GetDataField(const std::string &data_field_name,
                                            const std::string &target_frame = "") const;

  /**
   * @brief Triggers a particular event from this component. If the component has an
   *        event dispatcher attached to it, it will publish this event over ROS
   *        where it will be picked up by any remote observers.
   * @param event - an EventData describing the event that will be propagated
   *                to observers.
   */
  void TriggerEvent(EventData event) const;

  /**
   * @brief Attaches a functor to fire when a particular EventType event is triggered. Used
   *        for performing on-the-fly calcualtions.
   * @param trigger_event_type The event which triggers the functor (i.e. DataFieldUpdated)
   * @param trigger_data_var Any event meta-data that may condition the response (i.e. for
   * DataFieldUpdated meta-data could be the name of the data field that was updated. May be left
   * empty to catch all updates)
   * @param callback The method inside the desired functor to call (see examples)
   */
  void AddOnEventAction(EventData event_trigger, std::function<void()> callback);

  /**
   * @brief Attaches the tool required to publish events triggered by this component
   *        across ROS, so that remote observers can see them. By default, enables
   *        ROS forwarding for a component when attached.
   * @param dispatcher - an event dispatcher tool
   */
  void AttachEventDispatcher(std::shared_ptr<knowledge_interface::event_dispatch::EventDispatcher> dispatcher) {
    event_dispatcher_ = dispatcher;
    dispatch_events_ = true;
  }

  /**
   * @brief Sets whether this component should broadcast its events over ROS or not
   * @param status - true if the component should broadcast, false if it shouldn't
   */
  void SetDispatchEvents(bool status) { dispatch_events_ = status; }

  /**
   * @brief Attaches a reference to another component to this component (its "sibling")
   *        to support inter-component communications and data sharing.
   *        Siblings are added to the siblings_map_
   * @param component - the component to attach.
   */
  void AttachSibling(std::shared_ptr<components::BaseComponent> &component);

  /**
   * @brief Returns the map of component references attached to this component.
   * @return - the siblings_map_
   */
  std::map<std::string, std::shared_ptr<components::BaseComponent>> GetSiblings();

 private:
  /**
   * @brief Performs any heavy lifting of inserting data into whatever data structure
   *        we want to use to store DataFrames. For now, this uses a basic data_map_
   *        however in the future, this could be an insert into a mongo db or some
   *        other approach, and this is where that interface would go.
   * @param data_field_data the field to update
   * @param data_field_data the data to place into that field
   */
  void SetData(const std::string &data_field_name, const DataFrame<ComponentDataType> &data_field_data);

  /**
   * @brief Public-facing method to retrieve data from this component if possible,
   *        as above, in the future this would be the entry-point to some other data
   *        storage mechanism.
   * @param data_field_name The name of the data field to get
   * @param the requested data field, wrapped inside a DataFrame
   */
  DataFrame<ComponentDataType> GetData(const std::string &data_field_name) const;

  std::map<std::string, DataFrame<ComponentDataType>> data_map_;  //!< map storing all data about this component
  std::map<EventData, std::shared_ptr<boost::signals2::signal<void()>>> event_map_;  //!< a map of event triggers
  std::string label_;                                                                //!< the name of this component
  std::string parent_;                          //!< the name of this component's parent
  bool started_;                                //!< whether this component has been successfully started or not
  bool dispatch_events_;                        //!< whether this component should publish its events over ROS or not
  std::shared_ptr<tf2_ros::Buffer> tf_buffer_;  //!<  tf buffer for on-demand transforms
  std::shared_ptr<knowledge_interface::event_dispatch::EventDispatcher>
      event_dispatcher_;  //!< tool used to publish events over ROS
  std::map<std::string, std::shared_ptr<components::BaseComponent>>
      siblings_map_;  //!< map of all instantiated components
};
}  // namespace components
}  // namespace knowledge_base
}  // namespace glue
