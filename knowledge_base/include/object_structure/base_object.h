#pragma once
#include <object_structure/components/base_component.h>

namespace glue {
namespace knowledge_base {
/**
 * @brief base class for all objects, wraps components and common functionality.
 */
class BaseObject {
 public:
  using EventDispatcherPtr = std::shared_ptr<knowledge_interface::event_dispatch::EventDispatcher>;
  BaseObject(const std::string &object_label, const std::string &object_type, EventDispatcherPtr event_dispatcher)
      : label_(object_label), type_(object_type), model_started_(false), event_dispatcher_(event_dispatcher) {}

  /**
   * @brief Connects a component to this Object. Objects may have multiple components of the same type,
   *        but they must have unique names.
   * @param new_component the component to add
   */
  void AttachComponent(std::shared_ptr<components::BaseComponent> new_component);

  /**
   * @brief Retreives a component by name
   * @param component_label - the name of the component to return
   * @returns the requested component
   */
  std::shared_ptr<components::BaseComponent> GetComponent(const std::string &component_label);

  /**
   * @brief Detaches a component from this object model.
   */
  void DetachComponent(const std::string &component_label);

  /**
   * @brief Starts the Object as well as all of its components, in sequence.
   * @return true only if every component reported a successful start
   */
  bool Start();

  /**
   * @brief Stops the Object as well as all of its components, in sequence.
   * @return true only if every component attached to this object reported a successful stop
   */
  bool Stop();

  /**
   * @brief Updates the data field of a specific component of a specific object
   * @param object_label - the label of the object to update
   * @param component_label - the label of the component to update
   * @param component_data_field_label - the label of the data field to update
   * @param new_data - the data to insert
   */
  void UpdateComponentDataField(const std::string &component_label, const std::string &field_name,
                                const DataFrame<ComponentDataType> &new_data);

  /**
   * @brief Gets the name of this object
   * @return the object's label, as initialised
   */
  std::string const GetLabel() { return label_; }

  /**
   * @brief Gets the type of this object
   * @return the object's type, as initialised
   */
  std::string const GetType() { return type_; }

  /**
   * @brief Gets the started state of this model
   * @return true if the model has been started, false if it has not
   */
  bool HasStarted() const { return model_started_; }

  /**
   * @brief Returns a map of all components attached to this object
   * @return a map of components, where the key is the component label
   */
  std::map<std::string, std::shared_ptr<components::BaseComponent>> GetComponents() { return component_map_; }

 private:
  std::map<std::string, std::shared_ptr<components::BaseComponent>>
      component_map_;   //!< map of components attached to this object
  std::string label_;   //!< string label of this object
  std::string type_;    //!< string type of this object
  bool model_started_;  //!< started state of this object
  std::shared_ptr<knowledge_interface::event_dispatch::EventDispatcher>
      event_dispatcher_;  //!< interface for sending events to subscribers
};
}  // namespace knowledge_base
}  // namespace glue
