#pragma once
// system includes
#include <atomic>
#include <map>
#include <string>
#include <thread>
// ros includes
#include <ros/ros.h>
#include <std_srvs/Trigger.h>
// glue includes
#include <knowledge_base_msgs/DispatchEvent.h>
#include <knowledge_base_msgs/GetComponentDataField.h>
#include <knowledge_base_msgs/GetIsModelStarted.h>
#include <knowledge_base_msgs/SetComponentDataField.h>
#include <knowledge_base_msgs/StartStopModel.h>
#include <knowledge_interface/event_dispatcher.h>
#include <knowledge_interface/interface_types.h>
#include <object_structure/base_object.h>

namespace glue {
namespace knowledge_base {
/**
 * @brief The main class that handles providing the key services for all
 *        model interaction -- stopping, starting, instantiating etc.
 */
class KnowledgeCore {
 public:
  KnowledgeCore();
  /**
   * @brief Accesses the rosparam server, takes data about doors loaded there
   *        from yaml, and uses it to instantiate specific Doors in the KB.
   *        Should be extended and genericised in the future to replace spatial db
   *        and to support other object types.
   *
   *        Can be re-run via the service /knowledge_base/debug/reload which
   *        will update the state of the KB with whatever door data is on rosparam,
   *        allowing you to add/remove params, tweak data fields etc. all at runtime
   */
  void InitialiseModels();

  /**
   * @brief Inserts a newly instantiated object to the core data structure containing all
   *        active object models in the system. From there it can be queried.
   * @param new_object the new object to insert
   */
  void AttachObject(BaseObject &new_object);

  /**
   * @brief Retreives an object model from the core data structure.
   * @param object_label the label of the object to retrieve
   * @return the requested object if it exists
   */
  BaseObject &GetBaseObject(const std::string &object_label);

  /**
   * @brief Relays a component data field update to the right place
   * @param object_label - the label of the object to update
   * @param component_label - the label of the component to update
   * @param component_data_field_label - the label of the data field to update
   * @param new_data - the data to insert
   */
  void UpdateComponentDataField(const std::string &object_label, const std::string &component_label,
                                const std::string &component_data_field_label,
                                const DataFrame<ComponentDataType> &new_data);
  /**
   * @brief Starts a specific object model.
   * @return whether the model could start or not
   */
  bool StartModel(const std::string &object_label);

  /**
   * @brief Stops a specific object model.
   * @return whether the model could stop or not
   */
  bool StopModel(const std::string &object_label);

  /**
   * @brief ROS service handler for retrieving component data fields over ROS
   */
  bool HandleGetComponentDataFieldService(knowledge_base_msgs::GetComponentDataField::Request &request,
                                          knowledge_base_msgs::GetComponentDataField::Response &response);

  /**
   * @brief ROS service handler for setting component data fields over ROS
   */
  bool HandleSetComponentDataFieldService(knowledge_base_msgs::SetComponentDataField::Request &request,
                                          knowledge_base_msgs::SetComponentDataField::Response &response);

  /**
   * @brief ROS service handler for starting or stopping models over ROS
   */
  bool HandleToggleModelService(knowledge_base_msgs::StartStopModel::Request &request,
                                knowledge_base_msgs::StartStopModel::Response &response);

  /**
   * @brief ROS service handler for re-populating object models from rosparam
   */
  bool HandleReloadService(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response);

  /**
   * @brief ROS service handler for retrieving the started state of a model
   */
  bool HandleGetIsModelStartedService(knowledge_base_msgs::GetIsModelStarted::Request &request,
                                      knowledge_base_msgs::GetIsModelStarted::Response &response);

 private:
  std::map<std::string, BaseObject> object_map_;     //!< map of all instantiated objects
  std::shared_ptr<ros::NodeHandle> nh_;              //!< a handle to the ROS node used to provide services
  std::shared_ptr<ros::AsyncSpinner> spinner_;       //!< a spinner to manage the above nh
  ros::ServiceServer srv_get_component_data_field_;  //!< a ROS service to allow retrieval of data fields
  ros::ServiceServer srv_set_component_data_field_;  //!< a ROS service to allow setting of data fields
  ros::ServiceServer srv_start_or_stop_model_;       //!< a ROS service to allow starting or stopping an object model
  ros::ServiceServer srv_reload_from_rosparam;       //!< ROS service to allow reloading the rosparam
                                                     //!< state into active memory
  ros::ServiceServer srv_is_model_started_;          //!< ROS service to allow checking if a model is started
  std::shared_ptr<knowledge_interface::event_dispatch::EventDispatcher>
      event_dispatcher_;  //!< for publishing internal events over ROS
};
}  // namespace knowledge_base
}  // namespace glue