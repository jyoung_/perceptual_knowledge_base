#pragma once
// system includes
#include <string>
// glue includes
#include <knowledge_interface/models/common.h>
#include <object_structure/components/base_component.h>
#include <object_structure/components/common/common.h>
// microwave object component includes
#include <knowledge_interface/models/microwave/microwave.h>
#include <object_structure/components/microwave/control_buttons.h>
#include <object_structure/components/microwave/control_dial.h>
#include <object_structure/components/microwave/door.h>
namespace glue {
namespace knowledge_base {
namespace components {

/**
 * Factory handling the compilation of cpp objects from yaml-defined templates.
 */
class ObjectComponentFactory {
 public:
  static std::shared_ptr<BaseComponent> Create(const std::string &component_namespace,
                                               const std::string &component_label, const std::string &component_type,
                                               const std::string &parent_label, XmlRpc::XmlRpcValue component_params,
                                               std::shared_ptr<ros::NodeHandle> nh) {
    using namespace components;
    std::shared_ptr<BaseComponent> new_component;
    // TODO: Might be a scaling issue here, this class could be improved.
    // TODO: per-object type factories?
    ROS_INFO_STREAM(component_label);
    if (component_label == objects::common::kLabel) {
      new_component = std::make_shared<Common>(component_label, parent_label);
    }

    // microwave components
    if (component_label == objects::microwave::control_dial::kLabel) {
      new_component = std::make_shared<MicrowaveControlDial>(component_label, parent_label);
    }
    if (component_label == objects::microwave::control_buttons::kLabel) {
      new_component = std::make_shared<MicrowaveControlButtons>(component_label, parent_label);
    }
    if (component_label == objects::microwave::door::kLabel) {
      new_component = std::make_shared<MicrowaveDoor>(component_label, parent_label);
    }

    // add all yaml-specified params to the subject component of the subject model
    for (auto pdata_it = component_params.begin(); pdata_it != component_params.end(); pdata_it++) {
      // get the data from the rosparam server
      XmlRpc::XmlRpcValue param_data;
      if (nh->getParam(component_namespace + "/" + component_label + "/" + pdata_it->first, param_data)) {
        // rosparam already did the heavy lifting of determining the yaml var type
        // for us so we just now need to turn them into whatever types we
        // like then attach them to the component they were listed under
        new_component->SetDataField(pdata_it->first, DataFrame<ComponentDataType>(Convert(param_data)));
        ROS_INFO_STREAM("\t" << pdata_it->first << ": " << pdata_it->second);
      }
    }
    return new_component;
  }

  static ComponentDataType Convert(XmlRpc::XmlRpcValue data_val) {
    // TODO: This sort of stuff should be abstracted to its own module
    switch (data_val.getType()) {
      case XmlRpc::XmlRpcValue::TypeBoolean:
        return static_cast<bool>(data_val);
      case XmlRpc::XmlRpcValue::TypeInt:
        return static_cast<int>(data_val);
      case XmlRpc::XmlRpcValue::TypeDouble:
        return static_cast<double>(data_val);
      case XmlRpc::XmlRpcValue::TypeString:
        return static_cast<std::string>(data_val);
      case XmlRpc::XmlRpcValue::TypeArray:
        // rosparam doesn't distinguish between types of "lists of things"
        // so we have to just jump in here and tell it what we support:
        // could be a Pose (7 fields)
        if (data_val.size() == 7) {
          auto out_pose = geometry_msgs::Pose();
          out_pose.position.x = data_val[0];
          out_pose.position.y = data_val[1];
          out_pose.position.z = data_val[2];
          out_pose.orientation.x = data_val[3];
          out_pose.orientation.y = data_val[4];
          out_pose.orientation.z = data_val[5];
          out_pose.orientation.w = data_val[6];
          return out_pose;
        } else if (data_val.size() == 3) {
          // or could be a Vec3 (3 fields)
          auto out_vec = geometry_msgs::Vector3();
          out_vec.x = data_val[0];
          out_vec.y = data_val[1];
          out_vec.z = data_val[2];
          return out_vec;
        } else {
          // TODO: Support wider range of data structures if needed
          ROS_ERROR_STREAM("as-yet unsupported array type");
          return 0;  //  unsupported/unneeded as of yet
        }
      case XmlRpc::XmlRpcValue::TypeStruct:
        ROS_ERROR_STREAM("as-yet unsupported data type: Struct");
        return 0;  //  unsupported/unneeded as of yet
      case XmlRpc::XmlRpcValue::TypeDateTime:
        ROS_ERROR_STREAM("as-yet unsupported data type: DateTime");
        return 0;  // unsupported/unneeded as of yet
      case XmlRpc::XmlRpcValue::TypeBase64:
        ROS_ERROR_STREAM("as-yet unsupported data type: Base64");
        return 0;  //  unsupported/unneeded as of yet
      default:
        ROS_ERROR_STREAM("Unable to determine data type of field.");
        break;
    }
    return 0;  // Invalid value
  }
};
}  // namespace components
}  // namespace knowledge_base
}  // namespace glue
