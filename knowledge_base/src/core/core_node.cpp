#include <core/core.h>
#include <ros/ros.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "glue_knowledge_base_node");
  glue::knowledge_base::KnowledgeCore c;
  ros::waitForShutdown();
}
