#include <factories/object_component_factory.h>
// system includes
#include <fstream>
#include <iostream>
#include <string>
// ros includes
#include <ros/console.h>
#include <std_srvs/Empty.h>
// glue includes
#include <core/core.h>
#include <knowledge_base_msgs/DispatchEvent.h>
#include <knowledge_interface/data_utils.h>

namespace glue {
namespace knowledge_base {

KnowledgeCore::KnowledgeCore() {
  nh_ = std::make_shared<ros::NodeHandle>();
  event_dispatcher_ = std::make_shared<knowledge_interface::event_dispatch::EventDispatcher>();
  spinner_ = std::make_shared<ros::AsyncSpinner>(3);
  spinner_->start();
  srv_get_component_data_field_ = nh_->advertiseService("/knowledge_base/get_component_data_field",
                                                        &KnowledgeCore::HandleGetComponentDataFieldService, this);
  srv_set_component_data_field_ = nh_->advertiseService("/knowledge_base/set_component_data_field",
                                                        &KnowledgeCore::HandleSetComponentDataFieldService, this);
  srv_start_or_stop_model_ =
      nh_->advertiseService("/knowledge_base/start_or_stop_model", &KnowledgeCore::HandleToggleModelService, this);
  srv_reload_from_rosparam =
      nh_->advertiseService("/knowledge_base/debug/reload", &KnowledgeCore::HandleReloadService, this);

  srv_is_model_started_ =
      nh_->advertiseService("/knowledge_base/is_model_started", &KnowledgeCore::HandleGetIsModelStartedService, this);

  event_dispatcher_->Initialise(nh_);
  InitialiseModels();
}

void KnowledgeCore::InitialiseModels() {
  ROS_INFO("Initialising Model from rosparam");
  object_map_.clear();
  /* TODO:
   * This handles, in a slightly messy way for now, reading from a rosparam namespace
   * where initial object state will be found on system bringup, and ferrying that
   * data along to relevant factories that create KB objects.
   */
  std::string root_node = "/ObjectInstances/";
  std::string components_node = "/Components/";
  std::string common_node = "/Common/";
  std::string type_param_node = "type";
  XmlRpc::XmlRpcValue object_list;
  std::stringstream error_msg;
  nh_->getParam(root_node, object_list);
  // here get all the instances under the above namespace (root_node)
  for (auto olist_it = object_list.begin(); olist_it != object_list.end(); olist_it++) {
    // now we know to create a new door object
    auto object_label = olist_it->first;
    XmlRpc::XmlRpcValue type_param;
    if (!nh_->getParam(root_node + olist_it->first + components_node + common_node + type_param_node, type_param)) {
      error_msg << "unable to locate required type field for object " << olist_it->first;
      ROS_ERROR_STREAM(error_msg.str());
      throw std::runtime_error(error_msg.str());
    }
    // set up the new object with this label and type
    BaseObject new_object(object_label, type_param, event_dispatcher_);
    XmlRpc::XmlRpcValue components;
    ROS_INFO_STREAM("Instantiating " << new_object.GetLabel() << " of type " << new_object.GetType());
    nh_->getParam(root_node + olist_it->first + components_node, components);
    // next get the components of each door
    for (auto clist_it = components.begin(); clist_it != components.end(); clist_it++) {
      XmlRpc::XmlRpcValue component_params;
      nh_->getParam(root_node + olist_it->first + components_node + clist_it->first, component_params);
      ROS_INFO_STREAM("Creating component: " + clist_it->first);
      XmlRpc::XmlRpcValue component_type_param;
      if (!nh_->getParam(root_node + olist_it->first + components_node + "/" + clist_it->first + "/type",
                         component_type_param)) {
        error_msg << "unable to locate required type field for component " << clist_it->first
                  << " parent: " << olist_it->first;
        ROS_ERROR_STREAM(error_msg.str());
        throw std::runtime_error(error_msg.str());
      }
      ROS_INFO_STREAM("Basic information retreived, sending to factory for component construction.");
      auto new_component = components::ObjectComponentFactory::Create(root_node + olist_it->first + components_node,
                                                                      clist_it->first, component_type_param,
                                                                      new_object.GetLabel(), component_params, nh_);
      new_object.AttachComponent(new_component);
      ROS_INFO_STREAM("Done");
    }
    AttachObject(new_object);
  }
}

BaseObject &KnowledgeCore::GetBaseObject(const std::string &object_label) {
  if (object_map_.count(object_label) == 0) {
    std::stringstream ss;
    ss << "no object with label " << object_label << " instantiated";
    ROS_ERROR_STREAM(ss.str());
    throw std::runtime_error(ss.str());
  }
  return object_map_.at(object_label);
}

void KnowledgeCore::AttachObject(BaseObject &new_object) {
  // now let the components of each model know about their siblings
  for (auto &o_comp : new_object.GetComponents()) {
    for (auto &i_comp : new_object.GetComponents()) {
      if (i_comp.second->GetLabel() == o_comp.second->GetLabel()) {
        continue;
      } else {
        o_comp.second->AttachSibling(i_comp.second);
      }
    }
  }
  object_map_.insert(std::make_pair(new_object.GetLabel(), new_object));
}

void KnowledgeCore::UpdateComponentDataField(const std::string &object_label, const std::string &component_label,
                                             const std::string &component_data_field_label,
                                             const DataFrame<ComponentDataType> &new_data) {
  GetBaseObject(object_label).UpdateComponentDataField(component_label, component_data_field_label, new_data);
}

bool KnowledgeCore::StartModel(const std::string &object_label) { return GetBaseObject(object_label).Start(); }

bool KnowledgeCore::StopModel(const std::string &object_label) { return GetBaseObject(object_label).Stop(); }

bool KnowledgeCore::HandleSetComponentDataFieldService(knowledge_base_msgs::SetComponentDataField::Request &request,
                                                       knowledge_base_msgs::SetComponentDataField::Response &response) {
  try {
    if (!object_map_.at(request.object_label).HasStarted()) {
      ROS_ERROR_STREAM("Attempt to update object " << request.object_label << " failed as model is not yet started.");
      response.success = false;
      return true;
    }
    DataFrame<ComponentDataType> new_frame;
    new_frame.stamp = ros::Time::now();
    new_frame.frame_id = request.component_data_field.header.frame_id;
    new_frame.confidence = request.component_data_field.header.confidence;
    new_frame.content = {knowledge_interface::data_utils::GetDataFunc(request.component_data_field)};
    // TODO: Make updates transactional!!!!
    UpdateComponentDataField(request.object_label, request.component_label, request.component_data_field_label,
                             new_frame);
    response.success = true;
    ROS_DEBUG_STREAM("Parameter " << request.component_data_field_label << " in " << request.component_label << " of "
                                  << request.object_label << " set");
  } catch (const std::exception &ex) {
    ROS_ERROR_STREAM(ex.what());
    response.success = false;
    return true;
  }
  return true;
}

bool KnowledgeCore::HandleGetComponentDataFieldService(knowledge_base_msgs::GetComponentDataField::Request &request,
                                                       knowledge_base_msgs::GetComponentDataField::Response &response) {
  try {
    if (!GetBaseObject(request.object_label).HasStarted()) {
      ROS_WARN_STREAM("Attempt to access data of object model "
                      << request.object_label << " that is not yet started. Data may be out-of-date.");
    }
    DataFrame<ComponentDataType> data = GetBaseObject(request.object_label)
                                            .GetComponent(request.component_label)
                                            ->GetDataField(request.component_data_field_label, request.target_frame);
    // here handle a end-user defined on-demand transform
    response.component_data_field.header.stamp = data.stamp;
    response.component_data_field.header.frame_id = data.frame_id;
    response.component_data_field.header.confidence = data.confidence;

    std::visit(knowledge_interface::data_utils::SetDataVisitor(response.component_data_field), data.content.value());
    response.success = true;
  } catch (const std::exception &ex) {
    ROS_ERROR_STREAM(ex.what());
    response.success = false;
    return true;
  }
  return true;
}  // namespace knowledge_base

bool KnowledgeCore::HandleToggleModelService(knowledge_base_msgs::StartStopModel::Request &request,
                                             knowledge_base_msgs::StartStopModel::Response &response) {
  try {
    response.success = false;
    if (request.start) {
      ROS_INFO_STREAM("Attempting to start model");
      response.success = StartModel(request.object_label);
    } else {
      ROS_INFO_STREAM("Attempting to stop model");
      response.success = StopModel(request.object_label);
    }

    if (response.success) {
      ROS_INFO_STREAM("Success.");
    } else {
      ROS_INFO_STREAM("Failure.");
    }
  } catch (const std::exception &ex) {
    ROS_ERROR_STREAM(ex.what());
    return true;
  }
  return true;
}  // namespace knowledge_base

bool KnowledgeCore::HandleReloadService(std_srvs::Trigger::Request &request, std_srvs::Trigger::Response &response) {
  try {
    InitialiseModels();
    response.success = true;
  } catch (const std::exception &ex) {
    ROS_ERROR_STREAM(ex.what());
    response.success = false;
    return false;
  }
  return true;
}

bool KnowledgeCore::HandleGetIsModelStartedService(knowledge_base_msgs::GetIsModelStarted::Request &request,
                                                   knowledge_base_msgs::GetIsModelStarted::Response &response) {
  try {
    auto model = GetBaseObject(request.object_label);
    response.is_started = model.HasStarted();
    return true;
  } catch (const std::exception &ex) {
    ROS_ERROR_STREAM(ex.what());
    return false;
  }
}
}  // namespace knowledge_base
}  // namespace glue