#include <object_structure/base_object.h>

namespace glue {
namespace knowledge_base {

void BaseObject::AttachComponent(std::shared_ptr<components::BaseComponent> new_component) {
  if (component_map_.count(new_component->GetLabel()) != 0) {
    std::stringstream ss;
    ss << "component with label " << new_component->GetLabel() << " already attached to " << GetLabel();
    ss << "please choose a unique component label";
    ROS_ERROR_STREAM(ss.str());
    throw std::runtime_error(ss.str());
  }
  auto result = component_map_.try_emplace(new_component->GetLabel(), new_component);
  // check to see if map insertion was successful
  if (result.second) {
    new_component->AttachEventDispatcher(event_dispatcher_);
  } else {
    std::stringstream ss;
    ss << "unable to attach component with label " << new_component->GetLabel() << " to " << GetLabel();
    ROS_ERROR_STREAM(ss.str());
    throw std::runtime_error(ss.str());
  }
}

std::shared_ptr<components::BaseComponent> BaseObject::GetComponent(const std::string &component_label) {
  if (component_map_.count(component_label) == 0) {
    std::stringstream ss;
    ss << "no component with label " << component_label << " attached to " << GetLabel();
    ss << "please provide a valid component label";
    ROS_ERROR_STREAM(ss.str());
    throw std::invalid_argument(ss.str());
  }
  return component_map_.at(component_label);
}

bool BaseObject::Start() {
  if (model_started_) {
    ROS_ERROR_STREAM("Attempt to start object " << GetLabel() << " failed: Already started");
    return false;
  }
  for (const auto &component : component_map_) {
    if (!component.second->Start()) {
      ROS_ERROR_STREAM("Unable to start component: " << component.second->GetLabel());
      return false;
    }
  }
  event_dispatcher_->PublishEvent({EventType::ModelStart, "", GetLabel()});
  model_started_ = true;
  return true;
}

bool BaseObject::Stop() {
  if (!model_started_) {
    ROS_ERROR_STREAM("Attempt to stop object " << GetLabel() << " failed: Already stopped");
    return false;
  }
  for (const auto &component : component_map_) {
    if (!component.second->Stop()) {
      ROS_ERROR_STREAM("Unable to stop component: " << component.second->GetLabel());
      return false;
    }
  }
  event_dispatcher_->PublishEvent({EventType::ModelStop, "", GetLabel()});
  model_started_ = false;
  return true;
}

void BaseObject::UpdateComponentDataField(const std::string &component_label, const std::string &field_name,
                                          const DataFrame<ComponentDataType> &new_data) {
  GetComponent(component_label)->SetDataField(field_name, new_data);
}

}  // namespace knowledge_base
}  // namespace glue