#include <object_structure/components/base_component.h>

namespace glue {
namespace knowledge_base {
namespace components {

void BaseComponent::AddOnEventAction(EventData event_trigger, std::function<void()> callback) {
  // first, we may already have a signal to process this event, if not, create one
  if (event_map_.count(event_trigger) == 0) {
    event_map_[event_trigger] = std::make_shared<boost::signals2::signal<void()>>();
  }
  // connect the requested callback to the above signal at the requested event
  event_map_.at(event_trigger)->connect(callback);
}

void BaseComponent::AttachSibling(std::shared_ptr<components::BaseComponent> &component) {
  siblings_map_.emplace(component->GetLabel(), component);
}

DataFrame<ComponentDataType> BaseComponent::GetDataField(const std::string &data_field_name,
                                                         const std::string &target_frame) const {
  if (data_map_.count(data_field_name) == 0) {
    ROS_WARN_STREAM("Data field " << data_field_name << " may not yet exist.");
  }
  if (started_)
    TriggerEvent({EventType::DataFieldRequested, "", GetParentLabel(), GetLabel(), data_field_name});
  auto data = GetData(data_field_name);

  // handles on-demand transforms of requested data
  if (!target_frame.empty()) {
    std::string source_frame;
    // if this is empty, assume it's map frame, since it is probably from yaml
    if (data.frame_id == "") {
      ROS_INFO_STREAM("No frame set in source data, assuming annotated data in map frame.");
      source_frame = "map";
    } else {
      source_frame = data.frame_id;
    }
    ROS_INFO_STREAM("Attempting end-user defined on-demand transform.");
    ROS_INFO_STREAM("Target: " << target_frame << " Source: " << source_frame);
    // geometry_msgs::Pose source_pose = std::get<geometry_msgs::Pose>(data.content.value());
    geometry_msgs::TransformStamped transform;
    try {
      /*
      transform = tf_buffer_->lookupTransform(target_frame, source_frame, data.stamp, ros::Duration(5.0));
      geometry_msgs::Pose target_pose = tf_buffer_->transform(target_pose, source_frame);
      data.content = target_pose;
      data.stamp = transform.header.stamp;
      data.frame_id = target_frame;
      */
      ROS_INFO_STREAM("Transform completed.");
    } catch (tf2::TransformException &ex) {
      ROS_ERROR_STREAM("Transform between frames " << data.frame_id << " and " << target_frame << " unavailable");
      // give them back uninitialised data if the transform they wanted wasn't available
      data.content = std::nullopt;
    }
  }
  return data;
}

bool BaseComponent::HandleStart() { return true; }

bool BaseComponent::HandleStop() { return true; }

void BaseComponent::SetDataField(const std::string &data_field_name,
                                 const DataFrame<ComponentDataType> &data_field_data) {
  // TODO: Here, in the future, we can do some data validation
  SetData(data_field_name, data_field_data);
  if (started_)
    TriggerEvent({EventType::DataFieldUpdated, "", GetParentLabel(), GetLabel(), data_field_name});
}

bool BaseComponent::Start() {
  ROS_INFO_STREAM("Requesting start: " << label_);
  if (HandleStart()) {
    ROS_INFO_STREAM("Success");
    started_ = true;
    return true;
  }
  ROS_WARN_STREAM("Unable to start " << label_);
  return started_;
}

bool BaseComponent::Stop() {
  if (HandleStop()) {
    ROS_INFO_STREAM("Stopping " << label_);
    started_ = false;
    return true;
  }
  ROS_WARN_STREAM("Unable to stop " << label_);
  return false;
}

void BaseComponent::TriggerEvent(EventData event) const {
  // dispatch to internal subscribers via the signal api
  // alert any local listeners
  for (const auto &ev : event_map_) {
    if (ev.first == event) {
      // then trigger the signal, which will trigger all callbacks connected to it
      ev.second->operator()();
    }
  }

  // dispatch to external subscribers via the ROS api if we can
  if (dispatch_events_) {
    event_dispatcher_->PublishEvent(event);
  }
}

void BaseComponent::SetData(const std::string &data_field_name, const DataFrame<ComponentDataType> &data_field_data) {
  data_map_[data_field_name] = data_field_data;
}

DataFrame<ComponentDataType> BaseComponent::GetData(const std::string &data_var_name) const {
  return data_map_.at(data_var_name);
}

std::map<std::string, std::shared_ptr<components::BaseComponent>> BaseComponent::GetSiblings() { return siblings_map_; }

void BaseComponent::Visualise() {}

}  // namespace components
}  // namespace knowledge_base
}  // namespace glue
