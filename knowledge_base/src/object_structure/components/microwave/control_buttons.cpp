#include <object_structure/components/microwave/control_buttons.h>

namespace glue {
namespace knowledge_base {
namespace components {
MicrowaveControlButtons::MicrowaveControlButtons(const std::string &component_label, const std::string &parent_label)
    : BaseComponent(component_label, parent_label) {
  // example of an event-based action: whenever new data is received updating the pose of this object
  // the Visualize() method is called automatically -- allowing us to update state in RVIZ or other
  // front-end viewer
  AddOnEventAction({EventType::DataFieldUpdated, "", GetParentLabel(), component_label, "pose"},
                   std::bind(&MicrowaveControlButtons::Visualise, this));
}

void MicrowaveControlButtons::Visualise() {
  // TODO: fill out.
  // here we will define what needs to be done to publish the state of the object to, say, RVIZ
}
bool MicrowaveControlButtons::HandleStart() {
  // TODO: fill out
  return true;
}
bool MicrowaveControlButtons::HandleStop() {
  // TODO: fill out
  return true;
}
}  // namespace components
}  // namespace knowledge_base
}  // namespace glue