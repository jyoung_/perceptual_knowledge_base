#include <object_structure/components/microwave/control_dial.h>

namespace glue {
namespace knowledge_base {
namespace components {
MicrowaveControlDial::MicrowaveControlDial(const std::string &component_label, const std::string &parent_label)
    : BaseComponent(component_label, parent_label) {
  // example of an event-based action: whenever new data is received updating the pose of this object
  // the Visualize() method is called automatically -- allowing us to update state in RVIZ or other
  // front-end viewer
  AddOnEventAction({EventType::DataFieldUpdated, "", GetParentLabel(), component_label, "pose"},
                   std::bind(&MicrowaveControlDial::Visualise, this));
}

void MicrowaveControlDial::Visualise() {
  // TODO: fill out.
  // here we will define what needs to be done to publish the state of the object to, say, RVIZ
}
bool MicrowaveControlDial::HandleStart() {
  // TODO: fill out
  return true;
}
bool MicrowaveControlDial::HandleStop() {
  // TODO: fill out
  return true;
}
}  // namespace components
}  // namespace knowledge_base
}  // namespace glue