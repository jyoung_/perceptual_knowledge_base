#pragma once
// system includes
#include <iostream>
#include <string>
// ros includes
#include <ros/ros.h>
// glue includes
#include <knowledge_base_msgs/DispatchEvent.h>
#include <knowledge_interface/interface_types.h>

namespace glue {
namespace knowledge_interface {
namespace event_dispatch {
/**
 * First try at a system for sending events back and forth between the model and remote observers.
 * Uses a ROS Transport, requiring the end-user to keep a handle. So long as the handle is in scope
 * specified events will be received and forwarded on to a callback.
 */
class EventDispatcher {
 public:
  EventDispatcher();
  /**
   * @brief Delayed initialisation method for the event dispatcher
   * @param n a nodehandle
   */
  void Initialise(std::shared_ptr<ros::NodeHandle> n);
  /**
   * @brief Publishes an event on the ROS topic channel for remote observers to see
   * @param event state of event to publish
   */
  void PublishEvent(EventData event) const;
  /**
   * @brief Sets up a subscriber to a particular model event
   * @param desired_event - The specification of the event we want to subscribe to
   * @param external_callback - The callback method to call when this event fires
   * @param nh - a ROS nodehandle passed in by the user to help manage scope
   * @returns KnowledgeBaseEventHandle - a handle to the trigger/callback,
   *                                     unsubscribes from event when out of scope
   */
  static KnowledgeBaseEventHandle AttachEventObserver(EventData desired_event, LocalEventHandler external_callback,
                                                      ros::NodeHandle &nh) {
    // this is a wrapper function around the desired user-side callback
    // it subscribes the passed-in nodehandle to the event dispatch channel
    boost::function<void(const knowledge_base_msgs::DispatchEvent &msg)> internal_callback =
        [=](const knowledge_base_msgs::DispatchEvent &msg) {
          EventData triggered_event{static_cast<EventType>(msg.event_type), msg.event_channel, msg.object_label,
                                    msg.component_label, msg.component_data_field_label};
          // if the triggered event matches our specified conditions
          if (triggered_event == desired_event) {
            // call the specified callback, passing in the specific data about this event
            external_callback({triggered_event.event_type, triggered_event.event_channel, triggered_event.object_label,
                               triggered_event.component_label, triggered_event.component_data_field_label});
          }
        };
    return {desired_event,
            nh.subscribe<knowledge_base_msgs::DispatchEvent>("/knowledge_base/event_dispatch", 10, internal_callback)};
  }

 private:
  std::shared_ptr<ros::NodeHandle> nh_;         //!< a handle to the ROS node used to access services
  std::shared_ptr<ros::AsyncSpinner> spinner_;  //!< a spinner thread to manage the above nh
  ros::Publisher pub_event_dispatcher_;         //!< a ROS service to allow getting component data
};
}  // namespace event_dispatch
}  // namespace knowledge_interface
}  // namespace glue