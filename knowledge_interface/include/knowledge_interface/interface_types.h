#pragma once
// ros includes
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
// system includes
#include <optional>
#include <string>
#include <utility>
#include <variant>

/**
 * This file defines the types and enums used for communication between information annotators and the core.
 */

/**
 * @brief High-level event descriptors for event propagation.
 * These are used both internally for inter-component communication, and also can be transported
 * across the ROS bridge to alert end-users of the events defined below.
 */
enum class EventType {
  DataFieldUpdated, //!< Fired when any data field of a specified component is successfully updated
  DataFieldRequested, //!< Fired when any data field of a specified component is successfullyrequested
  ModelStart,         //!< Fired on startup of a model
  ModelStop,          //!< Fired on stop of a model
  InformationAnnotatorRequestStart,    //!< request to start information provider
  InformationAnnotatorRequestStop,     //!< request to stop information provider
  InformationAnnotatorResponseSuccess, //!< triggered by IP if it did what we asked
  InformationAnnotatorResponseFailure, //!< triggered by IP if it couldn't do what we asked
};

/**
 * Data type for specifying event notification triggers. This composes a set of
 * conditions to be specified across the various data fields. When an event that
 * meets these conditions is fired, the observer is notified with a EventResponse
 * struct that contains any extra data about the event.
 * Empty data fields are not considered.
 */
struct EventData {
  EventType event_type;        //!< represents one of the values in EventType
  std::string event_channel;  //!< optional: any meta-data associated with this event
  std::string object_label;    //!< optional: the object that should trigger this event
  std::string component_label; //!< optional: the component that should trigger this event
  std::string component_data_field_label; //!< optional: the data field that should trigger this event
  // basic thing required to use EventData as a map key
  bool operator<(const EventData &other) const {
    return std::tie(other.event_type, other.event_channel, other.object_label,
                    other.component_label, other.component_data_field_label) <
           std::tie(event_type, event_channel, object_label, component_label,
                    component_data_field_label);
  }  // we say that two event triggers are equal if they are the same along all of
  // the fields that are populated, empty fields are ignored
  bool operator==(const EventData &other) const {
    return other.event_type == event_type && ConditionMet(other.event_channel, event_channel) &&
           ConditionMet(other.object_label, object_label) &&
           ConditionMet(other.component_label, component_label) &&
           ConditionMet(other.component_data_field_label, component_data_field_label);
  }
  static bool ConditionMet(const std::string &other_state, const std::string &local_state) {
    // either this matches the desired value, or it's empty and we assume we don't care
    return (local_state.empty() || other_state.empty() || other_state == local_state);
  }
};

/**
 * Data type for specifying the return data to observers about a specific event
 * that was triggered. Observers to the event receive the following data structure
 * when the event they subscribed to (i.e. the one that meets the conditions of
 * their EventData) is fired.
 */
struct EventResponse {
  EventType event_type;        //!< represents one of the values in EventType
  std::string event_channel;  //!< optional: any meta-data associated with this event
  std::string object_label;    //!< optional: the object that triggered this event, if relevant
  std::string component_label; //!< optional: the component that triggered this event, if relevant
  std::string component_data_field_label; //!< optional: the data field that triggered this event, if relevant
};

/**
 * Overloaded ostream operator that helps printing EventResponse in logs.
 */
std::ostream &operator<<(std::ostream &os, const EventResponse &e);

/**
 * Definition of user-facing event handling callback function. This is what you
 * receive back when an event you are observing is fired. The event_data is a
 * EventData that tells you information (as above) about the event triggered.
 */
typedef std::function<void(EventResponse event_data)> LocalEventHandler;

/**
 * Definition of an event callback handle, so long as the end-user has this in
 * scope, their callback will fire when events of the specified type occur.
 */
struct KnowledgeBaseEventHandle {
  EventData event_trigger;
  ros::Subscriber subscriber;
};

/**
 * The union of datatypes that could be contained within a DataFrame.
 */
typedef std::variant<std::string, int, double, geometry_msgs::Pose, geometry_msgs::Vector3>
    ComponentDataType;

/**
 * A DataFrame provides context for a particular piece of data stored in the KB,
 * it also contains the actual data value itself.
 */
template <typename T>
struct DataFrame {
  DataFrame() {}
  DataFrame(T cont) : content(cont) {}
  ros::Time stamp = ros::Time(0);          //!< auto, assume uninitialised until inserted into model
  float confidence = 1.0;                  //!< the confidence of this data, if relevant
  std::string frame_id;                    //!< the frame of this data, if relevant
  std::optional<T> content = std::nullopt; //!< the data payload itself
  // this lets the end-user check to see if the data is initialised or not
  // exploits std::optional
  operator bool() const {
      return content.has_value();
  }
};
