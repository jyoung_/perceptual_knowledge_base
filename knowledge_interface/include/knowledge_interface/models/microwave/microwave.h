#pragma once
/*
 * This file provides const values for all door data fields. You can pass these
 * directly as parameters to the interface to access specific data fields etc.
 */
namespace objects {
namespace microwave {
namespace door {
static const std::string kLabel = "MicrowaveDoor";
static const std::string kPose = "pose";
}  // namespace door
namespace control_buttons {
static const std::string kLabel = "MicrowaveButtonsController";
static const std::string kPose = "pose";
}  // namespace control_buttons
namespace control_dial {
static const std::string kLabel = "MicrowaveDialController";
static const std::string kPose = "pose";
static const std::string kState = "state";
}  // namespace control_dial
}  // namespace microwave
}  // namespace objects
