#pragma once
/*
 * Static accessors for yaml-defined fields, so these can be used to request particular
 * peices of data from the model without having to handle strings.
 */
namespace objects {
namespace common {
static const std::string kLabel = "Common";
static const std::string kPose = "pose";
static const std::string kFloor = "floor";
}  // namespace common
}  // namespace objects
