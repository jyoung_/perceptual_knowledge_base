#pragma once
// system includes
#include <iostream>
#include <string>
#include <type_traits>
#include <variant>
// ros includes
#include <ros/ros.h>
// glue includes
#include <knowledge_base_msgs/GetComponentDataField.h>
#include <knowledge_base_msgs/SetComponentDataField.h>
#include <knowledge_base_msgs/StartStopModel.h>
#include <knowledge_interface/data_utils.h>
#include <knowledge_interface/event_dispatcher.h>

/**
 * The basic interface to the main knowledge_base, handles data retrieval and insertion
 */
namespace glue {
namespace knowledge_interface {
class KnowledgeInterface {
 public:
  KnowledgeInterface();
  /**
   * @brief Sets a single data field of a component including optional metadata in the DataFrame
   * @param object label - the name of the object, i.e. "Door 101"
   * @param component_label - the name of the object's component, i.e. "DoorFrame"
   * @param component_data_field_label - the data field of the component we want, i.e. "aperture"
   * @param new_data the data frame to insert at the location specified above
   * @return bool - whether data insertion was successful or not (note: insertion is transactional)
   *                it either succeeds completely or fails completely.
   */
  template <typename T>
  bool SetComponentDataField(const std::string &object_label, const std::string &component_label,
                             const std::string &component_data_field_label, const DataFrame<T> &new_data) const {
    knowledge_base_msgs::SetComponentDataField srv;
    srv.request.object_label = object_label;
    srv.request.component_label = component_label;
    srv.request.component_data_field_label = component_data_field_label;
    srv.request.component_data_field.header.frame_id = new_data.frame_id;
    srv.request.component_data_field.header.confidence = new_data.confidence;

    ComponentDataType data_field = new_data.content.value();
    // here this visitor figures out what data field to update, and how to convert this data
    // into a ROS Type so it can be sent over the communication channel
    std::visit(data_utils::SetDataVisitor(srv.request.component_data_field), data_field);
    // from here the ROS message is now populated with its data payload
    return srv_set_component_data_.call(srv);
  }

  /**
   * @brief Returns a single data field of a component, wrapped within a DataFrame that
   *        contains optional meta-data, like timestamp, confidence etc. if available
   * @param object label - the name of the object, i.e. "Door 101"
   * @param component_label - the name of the object's component, i.e. "DoorFrame"
   * @param component_data_field_label - the data field of the component we want, i.e. "aperture"
   * @param target_frame - optional frame to transform pose3d data to
   * @return a DataFrame containing the requested component, or an empty DataFrame if there was an
   * error
   */
  template <typename T>
  DataFrame<T> GetComponentDataField(const std::string &object_label, const std::string &component_label,
                                     const std::string &component_data_field_label,
                                     const std::string &target_frame = "") const {
    knowledge_base_msgs::GetComponentDataField srv;
    srv.request.object_label = object_label;
    srv.request.component_label = component_label;
    srv.request.component_data_field_label = component_data_field_label;
    srv.request.target_frame = target_frame;
    if (srv_get_component_data_.call(srv)) {
      DataFrame<T> output_data;
      // fill out the metadatas
      output_data.stamp = srv.response.component_data_field.header.stamp;
      output_data.frame_id = srv.response.component_data_field.header.frame_id;
      output_data.confidence = srv.response.component_data_field.header.confidence;
      // now figure out which of the response's data fields we need to package up based
      // on the T we got in this method call
      try {
        output_data.content = {std::get<T>(data_utils::GetDataFunc(srv.response.component_data_field))};
      } catch (const std::exception &ex) {
        ROS_WARN_STREAM("Request for " << component_data_field_label << " did not return any data.");
        ROS_ERROR_STREAM(ex.what());
      }
      // and we're done
      return output_data;
    } else {
      ROS_ERROR_STREAM("Unable to call get component srv. Fatal.");
      ROS_ERROR_STREAM("Is the core node running?");
    }
    // should reach here only on failure, in which case return blank, uninitialized data
    return DataFrame<T>();
  }

  /**
   * @brief Tells a particular object model to start.
   * @param object_label - the name of the object model to start
   * @return a boolean denoting whether this model started successfully or not
   */
  bool StartModel(const std::string &object_label) const;

  /**
   * @brief Tells a particular object model to stop.
   * @param object_label - the name of the object model to stop
   */
  bool StopModel(const std::string &object_label) const;

  /**
   * @brief Attaches a callback to a particular model event
   * @param event_trigger - the specification of the event to observe
   * @param callback - the callback function which will fire in response to the event
   * @param nh - a ROS nodehandle to manage scope
   * @returns a handle to this event observer, which will unsubscribe when out of scope
   */
  [[nodiscard]] KnowledgeBaseEventHandle AttachEventObserver(EventData event_trigger, LocalEventHandler callback,
                                                             ros::NodeHandle nh) const;
  /**
   * @brief Helper function for information providers to send back a response to the last
   *        action they were asked to perform (start/stop).
   * @param success -- whether the action was successful or not
   * @param target_provider -- the name of the provider that is responding, i.e.
   * "lidar_frame_tracker"
   */
  void TriggerInformationAnnotatorResponse(const std::string &target_provider, bool success) const;
  /**
   * @brief Helper function for requesting an information provider to start up. Does not block,
   *        any confirmation of successful start has to be handled manually.
   * @param target_provider -- the name of the provider to request to start, i.e.
   * "lidar_frame_tracker"
   * @param object label - the name of the object, i.e. "Door 101"
   * @param component_label - the name of the object's component, i.e. "DoorFrame"
   * @param component_data_field_label - the data field of the component we want, i.e. "aperture"
   */
  void LazyRequestInformationAnnotatorStart(const std::string &target_provider, const std::string &object_label,
                                            const std::string &component_label,
                                            const std::string &component_data_field_label) const;
  /**
   * @brief Helper function for requesting an information provider to stop
   * @param target_provider -- the name of the provider to stop, i.e. "lidar_frame_tracker"
   */
  void LazyRequestInformationAnnotatorStop(const std::string &target_provider) const;

  /**
   * @brief Helper function for requesting an information provider to start up. Blocks until
   *        a response is received.
   * @param target_provider -- the name of the provider to request to start, i.e.
   * "lidar_frame_tracker"
   * @param object label - the name of the object, i.e. "Door 101"
   * @param component_label - the name of the object's component, i.e. "DoorFrame"
   * @param component_data_field_label - the data field of the component we want, i.e. "aperture"
   * @param timeout -- max time to wait for information provider to respond before assuming failure
   * @param start -- true if we want the provider to start, false if we want it to stop
   * @param true if the information provider says it successfully started, false if not
   */
  bool BlockingRequestInformationAnnotatorStartStop(const std::string &target_provider, const std::string &object_label,
                                                    const std::string &component_label,
                                                    const std::string &component_data_field_label,
                                                    const ros::Duration &timeout, bool start, ros::NodeHandle nh) const;

 private:
  std::shared_ptr<ros::NodeHandle> nh_;                //!< a handle to the ROS node used to access services
  std::shared_ptr<ros::AsyncSpinner> spinner_;         //!< a spinner thread to manage the above nh
  mutable ros::ServiceClient srv_set_component_data_;  //!< a ROS service to allow setting component data
  mutable ros::ServiceClient srv_get_component_data_;  //!< a ROS service to allow getting component data
  event_dispatch::EventDispatcher event_dispatcher_;   //!< for sending and observing model events across ROS
};
}  // namespace knowledge_interface
}  // namespace glue