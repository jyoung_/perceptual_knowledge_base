#pragma once
// glue includes
#include <knowledge_base_msgs/GetComponentDataField.h>
#include <knowledge_interface/interface_types.h>
/**
 * These tools are used to help construct DataFrame structs and to convert
 * any local types into types that can be sent across ROS, and to convert
 * them back to the local types when they are received on the other side.
 */
namespace glue {
namespace knowledge_interface {
namespace data_utils {
/**
 * Since in ComponentDataField.msg we compress all of our data fields into a single
 * ROS msg, this visitor is used to populate the right fields of that msg with the
 * appropriate data, and convert any custom types to a format where they can be serialised
 * over ROS communications channels.
 */
struct SetDataVisitor {
 public:
  SetDataVisitor(knowledge_base_msgs::ComponentDataField &res) : data_(res) {}
  /**
   * @brief places double data in the ComponentDataField msg
   * @param the double we want to store
   */
  void operator()(double &d) const {
    data_.double_data = d;
    SetTypeHint("double");
  }

  /**
   * @brief places int data in the ComponentDataField msg
   * @param the int we want to store
   */
  void operator()(int &i) const {
    data_.int_data = i;
    SetTypeHint("int");
  }

  /**
   * @brief places string data in the ComponentDataField msg
   * @param the string we want to store
   */
  void operator()(std::string &s) const {
    data_.string_data = s;
    SetTypeHint("str");
  }

  /**
   * @brief places Pose data in the ComponentDataField msg
   * @param the Pose we want to store
   */
  void operator()(geometry_msgs::Pose &p) const {
    data_.pose_data = p;
    SetTypeHint("pose");
  }

  /**
   * @brief places a Vector3 in the ComponentDataField msg
   * @param the Pose we want to store
   */
  void operator()(geometry_msgs::Vector3 &v) const {
    data_.vec_data = v;
    SetTypeHint("vec");
  }

  /**
   * @brief Sets a string hint corresponding to which data field was updated
   *        by the visitor above.
   * @param the hint we want to store, corresponding to one of our datatypes
   */
  void SetTypeHint(const std::string &h) const { data_.type_hint = h; }

 private:
  knowledge_base_msgs::ComponentDataField &data_;  //!< the output msg we're trying to populate with data
};
/**
 * @brief This is the inverse of the above. It returns a data field based on the hint,
 * performing conversion from ROS types to whatever custom local types we might
 * want. This always converts whatever is in the msg to a ComponentDataType
 * @param data - a ComponentDataField ROS msg we wish to return data from based on the hint.
 */
ComponentDataType GetDataFunc(const knowledge_base_msgs::ComponentDataField &data);
}  // namespace data_utils
}  // namespace knowledge_interface
}  // namespace glue