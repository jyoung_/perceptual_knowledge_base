cmake_minimum_required(VERSION 3.0.2)
project(knowledge_interface)

set(PACKAGE_DEPENDENCIES
  knowledge_base_msgs
)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp ${PACKAGE_DEPENDENCIES}
)

catkin_package(
  CATKIN_DEPENDS ${PACKAGE_DEPENDENCIES}
  INCLUDE_DIRS include
  LIBRARIES knowledge_interface
)

include_directories(SYSTEM
  ${catkin_INCLUDE_DIRS}
)

include_directories(
  include
)

set(CMAKE_CXX_STANDARD 17)

add_compile_options(
  -Wall
  -Werror
  -pedantic
)

file(GLOB_RECURSE HEADERS ./include/*.h) # for clion indexing

## Declare a C++ library
add_library(knowledge_interface
  src/interface.cpp
  src/interface_types.cpp
  src/event_dispatcher.cpp
  src/data_utils.cpp
)
## Add dependencies
add_dependencies(knowledge_interface ${catkin_EXPORTED_TARGETS})

## Link the C++ library
target_link_libraries(knowledge_interface
  ${catkin_LIBRARIES}
)

add_executable(test_kb_interface src/test/test_kb_interface.cpp)
target_link_libraries(test_kb_interface knowledge_interface)

install(TARGETS
  knowledge_interface
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY include/
  DESTINATION ${CATKIN_GLOBAL_INCLUDE_DESTINATION}
)
