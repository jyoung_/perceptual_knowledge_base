#include <knowledge_interface/data_utils.h>

namespace glue {
namespace knowledge_interface {
namespace data_utils {
ComponentDataType GetDataFunc(const knowledge_base_msgs::ComponentDataField &data) {
  if (data.type_hint == "double") {
    return data.double_data;
  }
  if (data.type_hint == "int") {
    return data.int_data;
  }
  if (data.type_hint == "str") {
    return data.string_data;
  }
  if (data.type_hint == "pose") {
    return geometry_msgs::Pose(data.pose_data);
  }
  throw std::runtime_error("undefined type hint in msg");
}

}  // namespace data_utils
}  // namespace knowledge_interface
}  // namespace glue