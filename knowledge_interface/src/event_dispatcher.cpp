#include <knowledge_interface/event_dispatcher.h>

namespace glue {
namespace knowledge_interface {
namespace event_dispatch {
EventDispatcher::EventDispatcher() {}

void EventDispatcher::Initialise(std::shared_ptr<ros::NodeHandle> n) {
  nh_ = n;
  spinner_ = std::make_shared<ros::AsyncSpinner>(1);
  spinner_->start();
  pub_event_dispatcher_ =
      nh_->advertise<knowledge_base_msgs::DispatchEvent>("/knowledge_base/event_dispatch", 100);
}

void EventDispatcher::PublishEvent(EventData event) const {
  knowledge_base_msgs::DispatchEvent msg;
  msg.event_type = static_cast<int>(event.event_type); // cast because of typed enum
  msg.event_channel = event.event_channel;
  msg.object_label = event.object_label;
  msg.component_label = event.component_label;
  msg.component_data_field_label = event.component_data_field_label;
  pub_event_dispatcher_.publish(msg);
}
} // namespace event_dispatch
} // namespace knowledge_interface
} // namespace glue