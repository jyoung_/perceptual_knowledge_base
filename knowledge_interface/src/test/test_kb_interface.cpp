#include <knowledge_interface/interface.h>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

/**
 *
 * This executable demonstrates some of the basic functionality of the perceptual knowledge base and
 * the supplied interface APIs. The dummy data is supplied by the microwave definitions in the
 * test_objects.yaml file under knowledge_base/test/, which is read in to rosparam when launch_kb.launch
 * is executed.
 *
 * Each section is documented, detailing what use-case is being demonstrated.
 *
 */

// used to demonstrate the event system
// this callback fills in the place of an information annotator that may live in its own node
// or elsewhere in the system
void some_callback(EventResponse event_trigger) {
  std::cout << "> A callback was triggered!" << std::endl;
  std::cout << "> I've been asked to detect information about: " << event_trigger.object_label << std::endl;
  std::cout << "> I should send my data to its component: " << event_trigger.component_label << std::endl;
  std::cout << "> in the data field: " << event_trigger.component_data_field_label << std::endl;
  // could do this, or keep interface as a class member
  std::cout << "> waiting 3s before I reply..." << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(3));
  glue::knowledge_interface::KnowledgeInterface my_local_interface;
  std::cout << "> replying now that I successfully started!" << std::endl;
  // here the information annotator is simply replying to say that that it started successfully and
  // is now populating data about the object it was requested to work on
  my_local_interface.TriggerInformationAnnotatorResponse("microwave_dial_state_detector", true);
  std::cout << "" << std::endl;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "test_kb_interface");
  glue::knowledge_interface::KnowledgeInterface knowledge_interface;

  /*
   * These first two chunks demonstrate basic setting and getting of data structures to/from components
   */

  std::cout << "Basic Setting and Getting" << std::endl;
  std::cout << "----" << std::endl;
  // here we have the simplest thing, we want to know the pose of kitchen_4_microwave
  auto microwave_pose_df =
      knowledge_interface.GetComponentDataField<geometry_msgs::Pose>("kitchen_4_microwave", "Common", "pose");
  // we can check to see if this data is initialised or not, as we use std::optional
  // in this case the data will always be initialised, since a value is present in the yaml definition
  // but, in case someone forgot to put it there, we can check for initialisation
  if (microwave_pose_df.content.has_value()) {
    // if the data is initialised we can get it out like this
    geometry_msgs::Pose microwave_pose_data = microwave_pose_df.content.value();
    std::cout << "kitchen_4_microwave pose is: " << microwave_pose_data << std::endl;
  } else {
    // we might throw some error here
  }

  std::cout << "confidence: " << microwave_pose_df.confidence << std::endl;
  std::cout << "timestamp: " << microwave_pose_df.stamp
            << std::endl;  // this will be 0, since this is initialised from yaml
  std::cout << "----" << std::endl;

  // next up: let's change the stored pose
  // first we need to start that model so that it is capable of receiving new data
  knowledge_interface.StartModel("kitchen_4_microwave");
  // this is how an information annotator might push new data to this object
  DataFrame<geometry_msgs::Pose> new_microwave_pose_df;
  new_microwave_pose_df.content = geometry_msgs::Pose();  // replace the existing data with an empty pose
  new_microwave_pose_df.confidence = 0.5;                 // and now we say we have 0.1 confidence in the result

  // now we update that component with the new pose
  knowledge_interface.SetComponentDataField<geometry_msgs::Pose>("kitchen_4_microwave", "Common", "pose",
                                                                 new_microwave_pose_df);
  // and let's get the component back and check to see if those modifications worked
  microwave_pose_df =
      knowledge_interface.GetComponentDataField<geometry_msgs::Pose>("kitchen_4_microwave", "Common", "pose");
  std::cout << "kitchen_4_microwave pose is now: " << microwave_pose_df.content.value() << std::endl;
  std::cout << "confidence:" << microwave_pose_df.confidence << std::endl;
  std::cout << "timestamp:" << microwave_pose_df.stamp << std::endl;
  std::cout << "----" << std::endl;
  std::cout << "" << std::endl;

  /*
   * The above covered the basics of getting and setting data into the KB. Next we will
   * look at the event system. First, we are going to set up an event that will allow us to
   * tell an imaginary annotator called "microwave_dial_state_detector" that we need it to start
   * doing work for us.
   */

  // ROS nodehandle to keep scope
  ros::NodeHandle nh;
  // now we define the type of event we want to listen for
  EventData start_request_event;
  // events have a type as well as a channel, the type we want here is start event
  start_request_event.event_type = EventType::InformationAnnotatorRequestStart;
  // our dial state detector is going to listen for start events on the microwave_dial_state_detector channel
  start_request_event.event_channel = "microwave_dial_state_detector";
  // these are simply strings used to direct the right events to the right observers
  // and finally we attach all the above and get back a handle
  KnowledgeBaseEventHandle handle = knowledge_interface.AttachEventObserver(start_request_event, some_callback, nh);
  // so long as the above handle is in scope, when InformationAnnotatorRequestStart events are fired on the
  // microwave_dial_state_detector channel, the above callback method some_callback will be triggered.
  // this code would be inside an information annotator, and be used to order it to begin doing work.

  // now we're done setting up the event, let's see what happens when someone triggers it

  // in our code here, we're going to optionally block while we wait for the above information annotator to receive
  // and acknowledge our start request. we could optionally also *not* block and receive a callback event when
  // the IA tells us it is ready
  bool response = knowledge_interface.BlockingRequestInformationAnnotatorStartStop(
      "microwave_dial_state_detector", "kitchen_7_microwave", "MicrowaveDialController", "state", ros::Duration(10),
      true, nh);
  // in the above call, the information annotator receives the data about the object we want it to tell us about
  // this is so that, when it begins entering information in the KB, it knows where to put it. We can even optionally
  // specify which specific model parameters it should populate.

  // here we should see the output from the some_callback method

  // and finally we can see whether it worked
  std::cout << "blocking call finished, result: " << std::endl;
  if (response) {
    std::cout << "information annotator started successfully!" << std::endl;
  } else {
    std::cout << "information annotator start failure!" << std::endl;
  }

  // finally we can shut down the model and turn off any information annotators that might
  // still be running
  knowledge_interface.StopModel("kitchen_4_microwave");
  std::cout << "Demo finished! Ctrl+C to quit." << std::endl;
  while (ros::ok()) {
    // block here so we don't lose the event handles etc.
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
}
