#include <knowledge_interface/interface.h>
#include <chrono>
#include <thread>
namespace glue {
namespace knowledge_interface {

KnowledgeInterface::KnowledgeInterface() {
  nh_ = std::make_shared<ros::NodeHandle>();
  spinner_ = std::make_shared<ros::AsyncSpinner>(1);
  spinner_->start();
  event_dispatcher_.Initialise(nh_);
  srv_set_component_data_ =
      nh_->serviceClient<knowledge_base_msgs::SetComponentDataField>("/knowledge_base/set_component_data_field");
  srv_get_component_data_ =
      nh_->serviceClient<knowledge_base_msgs::GetComponentDataField>("/knowledge_base/get_component_data_field");
}

bool KnowledgeInterface::StartModel(const std::string &object_label) const {
  ros::NodeHandle nh;
  ros::ServiceClient client =
      nh.serviceClient<knowledge_base_msgs::StartStopModel>("/knowledge_base/start_or_stop_model");
  knowledge_base_msgs::StartStopModel srv;
  srv.request.object_label = object_label;
  srv.request.start = true;
  if (client.call(srv)) {
    return srv.response.success;
  }
  return false;
}

bool KnowledgeInterface::StopModel(const std::string &object_label) const {
  ros::NodeHandle nh;
  ros::ServiceClient client =
      nh.serviceClient<knowledge_base_msgs::StartStopModel>("/knowledge_base/start_or_stop_model");
  knowledge_base_msgs::StartStopModel srv;
  srv.request.object_label = object_label;
  srv.request.start = false;
  if (client.call(srv)) {
    return srv.response.success;
  }
  return false;
}

KnowledgeBaseEventHandle KnowledgeInterface::AttachEventObserver(EventData event_trigger, LocalEventHandler callback,
                                                                 ros::NodeHandle nh) const {
  return event_dispatcher_.AttachEventObserver(event_trigger, callback, nh);
}

void KnowledgeInterface::TriggerInformationAnnotatorResponse(const std::string &target_provider, bool success) const {
  if (success) {
    event_dispatcher_.PublishEvent({EventType::InformationAnnotatorResponseSuccess, target_provider});
  } else {
    event_dispatcher_.PublishEvent({EventType::InformationAnnotatorResponseFailure, target_provider});
  }
}

bool KnowledgeInterface::BlockingRequestInformationAnnotatorStartStop(
    const std::string &target_provider, const std::string &object_label, const std::string &component_label,
    const std::string &component_data_field_label, const ros::Duration &timeout, bool start, ros::NodeHandle nh) const {
  std::atomic<bool> received_response;
  std::atomic<bool> success;
  received_response = false;
  success = false;
  LocalEventHandler internal_callback = [&received_response, &success](EventResponse event_response) {
    if (event_response.event_type == EventType::InformationAnnotatorResponseSuccess) {
      success = true;
    }
    if (event_response.event_type == EventType::InformationAnnotatorResponseFailure) {
      success = false;
    }
    received_response = true;
  };
  // need to keep these handles in scope, or else we will immediately unsubscribe from the event
  auto success_handle =
      AttachEventObserver({EventType::InformationAnnotatorResponseSuccess, target_provider}, internal_callback, nh);
  auto failure_handle =
      AttachEventObserver({EventType::InformationAnnotatorResponseFailure, target_provider}, internal_callback, nh);
  if (start) {
    LazyRequestInformationAnnotatorStart(target_provider, object_label, component_label, component_data_field_label);
  } else {
    LazyRequestInformationAnnotatorStop(target_provider);
  }

  ros::Rate r(1);  // 1 hz
  ros::Time end_time = ros::Time::now() + timeout;
  while (!received_response && ros::ok() && ros::Time::now() < end_time) {
    r.sleep();
  }
  return success;
}

void KnowledgeInterface::LazyRequestInformationAnnotatorStart(const std::string &target_provider,
                                                              const std::string &object_label,
                                                              const std::string &component_label,
                                                              const std::string &component_data_field_label) const {
  event_dispatcher_.PublishEvent({EventType::InformationAnnotatorRequestStart, target_provider, object_label,
                                  component_label, component_data_field_label});
}

void KnowledgeInterface::LazyRequestInformationAnnotatorStop(const std::string &target_provider) const {
  event_dispatcher_.PublishEvent({EventType::InformationAnnotatorRequestStop, target_provider});
}

}  // namespace knowledge_interface
}  // namespace glue
