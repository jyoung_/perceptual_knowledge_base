#include <knowledge_interface/interface_types.h>

 std::ostream &operator<<(std::ostream &os, const EventResponse &e) {
  os << "event_channel: " << std::quoted(e.event_channel)
     << ", object_label: " << std::quoted(e.object_label)
     << ", component_label: " << std::quoted(e.component_label)
     << ", component_data_field_label: " << std::quoted(e.component_data_field_label);
  return os;
}